#### Events

### **Event Tracking APIs**

> All **Analytics/Events** related API's are part of WebEngage Android SDK's  **`Analytics`** Object.

You get an instance of WebEngage `Analytics` object as follows:
```java
// import WebEngage 'Analytics'
import com.webengage.sdk.android.Analytics;

// Get an instance of `Analytics` object
Analytics weAnalytics = WebEngage.get().analytics();
```

After WebEngage has been successfully initialized you can track an event by using following apis on `Analytics` Object:
```java
weAnalytics.track(String eventName)

weAnalytics.track(String eventName, Map<String,? extends Object> attributes)
// Allowed data types for map values: `String`, `Integer`, `Long`, `Double`, `Boolean`, `List`.
// The `List` itself can contain one of these data types: `String`, `Integer`, `Long`, `Double`, `Boolean`
```
