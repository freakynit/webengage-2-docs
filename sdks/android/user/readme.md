#### User

### **User APIs**
WebEngage allows you to attach attributes to logged-in as well as anonymous users.

Attributes are a set of properties that make up a user profile. This profile information can be used to segment users which allows for better targeting based on different user personas.

> All **User** related API's are part of WebEngage Android SDK's  **`User`** Object.

You get an instance of WebEngage `User` object as follows:
```java
// import WebEngage 'User'
import com.webengage.sdk.android.User;

// Get an instance of `User` object
User weUser = WebEngage.get().user();
```

#### **User Identification**

Use following API to identify a user to WebEngage:

**When a user Logs-In (`Identifying User`)**
```java
weUser.loggedIn(String identifer)
```

> **IMP:** Please make sure to call above api as soon as a user logs into your application, or whenever earliest you are able to identify him/her.

> Once called, every session, user attribute and event will be attributed to this user.

> All attributes, events and session information accumulated before this API has been called gets attributes to an anonymous user created by default. Once this anonymous user is identified (by using `loggedIn()` API call), all of this stored information is attributed to this `loggedIn` (**identified**) user.

**When a user Logs-Out (`Forgetting User`)**

When a user logs out of your application then
```java
weUser.loggedOut()
```
> Call this when the `logged-in` user logs-out, or, you do not want to attach any future event, session and/or user data with this user, unless `loggedIn` is called again.

#### **Setting Custom User Attributes**
> Use these API's to attach custom attributes to the user

```java
weUser.setAttribute(String attributeName,String value)

weUser.setAttribute(String attributeName,Boolean value)

weUser.setAttribute(String attributeName,Long value)

weUser.setAttribute(String attributeName,Double value)

weUser.setAttribute(String attributeName,Integer value)

weUser.setAttribute(String attributeName,List<? extends Object> values)
```

> **IMP:** Following java data types are allowed for above API's: `String`, `Integer`, `Long`, `Double`, `Boolean`. The `List` itself can contain one of these data types.

```java
weUser.setAttributes(Map<String,? extends Object> attributes)
```
> The values in above `Map` should only be one of the above defined data types, or a `List`. If the value is of `List` type, it should not contain any other data type other than defined above.

<br />
#### **Setting Pre-Defined User Attributes**
```java
weUser.setEmail(String email)

weUser.setBirthDate(Integer year,Integer month,Integer day) // Month is index 1 based: January = 1, ...

weUser.setPhoneNumber(String phoneNumber)

weUser.setGender(String gender) // Can be `male`, `female` or `other`

weUser.setFirstName(String firstName)

weUser.setLastName(String lastName)

weUser.setCompany(String company)

weUser.setOptIn(Channel channel, boolean state); // Set user's opt_in status. Channel can be one of the channels declared in "Channel" class.

weUser.setUserProfile(UserProfile userProfile) // Use this to set user profile in one go using `Java Builder Pattern`
```

#### **Managing Users' Opt_In Status**
Use following API on `User` object to set **opt_in** status of a user for a particular `channel`:
```java
weUser.setOptIn(Channel channel, boolean state);
```
> See [Channel](#channel) for details

You can use this API to set users' **opt_in** preference to a `Channel`. Channel could be **Push Notifications**, SMS(*coming soon*), etc.. Once a user **opts-out** of a channel, he/she will not receive any message on that channel.  

This is also reflected in your dashboard where you can see how many users have **opted-out** (from a particular channel), and can be used in targeting also.
> Default value: `true`

#### **Deleting Custom User Attributes**
```java
weUser.deleteAttribute(String attributeKey)

weUser.deleteAttributes(List<String> attributeKeys)
```

#### **Deleting System User Attributes**
```java
weUser.deleteAttribute(UserAttributeKey attributeKey)
```
