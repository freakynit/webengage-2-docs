#### Manifest Changes

#### 1. Add following meta-data under manifest tag of your AndroidManifest.xml:
```xml
<meta-data
  android:name="com.webengage.sdk.android.project_number"
  android:value="$YOUR_GCM_PROJECT_NUMBER" />
<meta-data
  android:name="com.webengage.sdk.android.key"
  android:value="YOUR_WEBENGAGE_KEY" />
```
> **NOTE:** Make sure to prefix “YOUR_GCM_PROJECT_NUMBER” with a ‘$’ sign.


#### 2. Add following services under application tag of your AndroidManifest.xml:
```xml
// required for event processing
<service android:name="com.webengage.sdk.android.ExecutorService" />

// required for event logging
<service android:name="com.webengage.sdk.android.EventLogService" />
```


#### 3. Add following permissions under manifest tag:
```xml
// required by WebEngage
<uses-permission android:name="android.permission.INTERNET"/>

// optional,but highly recommended
<uses-permission android:name="android.permission.WAKE_LOCK" />  
```


#### 4. GCM SETUP:
> Before set-up, make sure to include Google Play Services in your project.
(Refer: https://developers.google.com/android/guides/setup for more details)

##### 4.1. If GCM Registration for push messaging should be handled by WebEngage SDK, add following to your manifest:
  1. **meta-data**

  ```xml
  <meta-data
    android:name="com.webengage.sdk.android.auto_gcm_registration"
    android:value="true" />
  ```
  2. **permissions**

  ```xml
  <uses-permission android:name="com.google.android.c2dm.permission.RECEIVE" />
  <permission
    android:name="YOUR.PACKAGE.NAME.permission.C2D_MESSAGE"
    android:protectionLevel="signature" />
  <uses-permission android:name="YOUR.PACKAGE.NAME.permission.C2D_MESSAGE" />
  ```
  3. **receiver**

  ```xml
  <receiver android:name="com.webengage.sdk.android.WebEngageReceiver"
    android:permission="com.google.android.c2dm.permission.SEND">
    <intent-filter>
      <action android:name="com.google.android.c2dm.intent.RECEIVE" />
        <action android:name="com.webengage.sdk.android.intent.ACTION" />
        <category android:name="YOUR.PACKAGE.NAME" />
      </intent-filter>
  </receiver>
  ```
  > **NOTE:** Replace YOUR.PACKAGE.NAME with your package name


##### 4.2. Else, if GCM Registration is already being handled by your app, or you plan to handle it yourself, add following to your manifest:
  1. **meta-data**

  ```xml
  <meta-data
    android:name="com.webengage.sdk.android.auto_gcm_registration"
    android:value="false" />
  ```

  2. **receiver**

  ```xml
  <receiver
    android:name="com.webengage.sdk.android.WebEngageReceiver"
    <intent-filter>
      <action android:name="com.webengage.sdk.android.intent.ACTION" />
      <category android:name="YOUR.PACKAGE.NAME" />
    </intent-filter>
  </receiver>
  ```


#### 5. LOCATION TRACKING SET-UP:
Add following meta-data under manifest tag of your AndroidManifest.xml:

```xml
  // If WebEngage should enable location tracking, set it to true, otherwise set it to false. Default: TRUE
  <meta-data
    android:name="com.webengage.sdk.android.location_tracking"
    android:value="true" />
```
> If  above meta-data is set to true then add following permission under manifest tag of your
  `AndroidManifest.xml`:

```xml
  <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
```


#### 6. For WebEngage Attribution tracking, set-up an INSTALL-REFERRER:

* (6.1) If WebEngage is the only SDK/Library you are using to track installs, it is enough to just add following to your manifest (to setup   `WebEngage's InstallReferrer`)

```xml
<receiver
  android:name="com.webengage.sdk.android.InstallTracker"
  android:exported="true">
  <intent-filter>
    <action android:name="com.android.vending.INSTALL_REFERRER" />
  </intent-filter>
</receiver>
```

* (6.2) However, if you have an `InstallReferrer` already set up, and do not want to change it, or you want some other `InstallReferrer` to be the **`primary one`**, you can still use `WebEngage's InstallReferrer` for `Attribution Tracking` by passing on the `Intent` Data received in your primary InstallReferrer's `Broadcast Receiver` to WebEngage's InstallReferrer's `onReceive()`.  Use following API for this:
```java
WebEngage.get().analytics().onInstalled(intent);
// "intent" is same as received in broadcast receiver
```

**Example:**
```java
public class PrimaryInstallTracker extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // Your existing stuff

        // Call WebEngage's onInstalled() passing on the received intent
        WebEngage.get().analytics().onInstalled(intent);
    }
}
```

> **Note:** In case the `Primary InstallReferrer` is part of some `library`, whose code you cannot change, or there is some other issue that prevents you from accessing/changing the existing `InstallReferrer` code, we suggest you to setup a `new InstallReferrer`, make it primary, and then call `library's InstallReferrer` and `WebEngage's InstallReferrer` from this new primary InstallReferrer. Doing so is extremely straight forward.
You'll just need to set up a new InstallReferrer, declare it in manifest, and from the `onReceive()` of this new `Primary InstallReferrer` call `library's` and `WebEngage's` InstallReferrers one by one, and possibly others too, if there are even more libraries/SDK's.



#### 7. Enabling/disabling Debug Mode:
```xml
<meta-data
  android:name="com.webengage.sdk.android.debug"
  android:value="true" />
```
> Can be **`true`** or **`false`.**

> When debug mode is `true` debug logs from the SDK are printed onto console.

> These logs shows various activities happening inside the SDK which are useful for the app developer, such as presence of compulsory permissions, meta tags, services, receivers, etc. required by the SDK, events being tracked, logged-in user identifier, etc.

> Log-level can be set using WebEngage's logging API. Refer: [API Section for more details](../API/index.html "Logging API")
