#### Basic SDK Integration

#### 1. Automatic
This approach is only supported in `Android 4.0`(API level 14, >92% coverage as of August 2015) and later. So, if you are developing your app to support minimal `14 API level`, we **recommend** to use this approach as it is easy to integrate and requires less boilerplate.

##### 1.1 If you don't have a custom Application class, create one and specify the name in your AndroidManifest.xml

```xml
  <application
    android:name=".MyApplication"
    android:icon="@drawable/ic_launcher"
    android:label="@string/app_name">
```

##### 1.2. Now, In your Application's `onCreate()` method, register `WebEngageActivityLifeCycleCallbacks`.

```java
  public class MyApplication extends Application {
    @Override
    public void onCreate() {
      super.onCreate();
      // Register WebEngageActivityLifeCycleCallabcks
      registerActivityLifecycleCallbacks(new WebEngageActivityLifeCycleCallbacks(this));
    }
  }
```

#### 2. Manual
Use this approach if you plan to target devices `below Android API level 14 also` ( >94% coverage as of August 2015)

##### 1.1 If you don't have a custom Application class, create one, and specify the name in your AndroidManifest.xml

```xml
  <application
    android:name=".MyApplication"
    android:icon="@drawable/ic_launcher"
    android:label="@string/app_name">
```

##### 1.2 Now, In your application's `onCreate()` method, initialize `WebEngage`:

```java
public class MyApplication extends Application {
  @Override
  public void onCreate() {
    super.onCreate();
    // Integrate WebEngage
    WebEngage.engage(this);
  }}
```

##### 1.3 Now, to enable `users' session tracking`, do the following for **each `Activity`**:

From `onResume()` of Activity call `onResume(ACTIVITY_CONTEXT) on WebEngage`:

```java
@Override
public void onResume(){
  super.onResume();
  WebEngage.get().analytics().onResume(this);
}
```

From `onPause()` of Activity call `onPause(ACTIVITY_CONTEXT) of WebEngage`:

```java
public void onPause(){
  super.onPause();
  WebEngage.get().analytics().onPause(this);
}
```

**NOTE:** Make sure to repeat above 2 steps(the onResume() and onPause()) for every other Activity in your application. This enables user session tracking and funnel generation for your app visitors.

> Session tracking also logs the current activity the user is in. This can be used for example in games to track what all stages a user has went through. This is immensely helpful in funnel generation too where you can easily see where most of your users spend their times, and where they drop.

#### Disabling Session Tracking
If for some reason you would like to disabled session tracking for some activities (screens), you can do that by simply not calling `WebEngage.get().analytics().onResume(this);` and `WebEngage.get().analytics().onPause(this);` in corresponding activities.
**But**, do make sure to keep them in sync. For every `WebEngage's onResume()` call, there should be a corresponding `onPause()` call.

> **Note:** This is not a recommended practise though as it breaks the user funnel.
