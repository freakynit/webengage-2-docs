#### Integration

Once you sign-up and integrate [Webengage Widget Code](http://webengage.com/integrate-with/your-website "Webengage Widget Code") on your web-site, you can use this Javascript API to change the default behaviour of how the Surveys, Feedback and Notifications work on your site to suit your needs.

## Integration Code and API

### Default Integration Code

Following is the default **one time integration code** snippet -

	<script id="_webengage_script_tag" type="text/javascript">
		!function(e,t,n){function o(e,t){e[t[t.length-1]]=function(){r.__queue.push([t.join("."),arguments])}}var i,s,r=e[n],g=" ",l="init options track onReady".split(g),a="feedback survey notification".split(g),c="options render clear abort".split(g),p="Open Close Submit Complete View Click".split(g),u="identify login logout setAttribute".split(g);if(!r||!r.__v){for(e[n]=r={__queue:[],__v:"5.0",user:{}},i=0;i<l.length;i++)o(r,[l[i]]);for(i=0;i<a.length;i++){for(r[a[i]]={},s=0;s<c.length;s++)o(r[a[i]],[a[i],c[s]]);for(s=0;s<p.length;s++)o(r[a[i]],[a[i],"on"+p[s]])}for(i=0;i<u.length;i++)o(r.user,["user",u[i]]);var f=t.createElement("script"),d=t.getElementById("_webengage_script_tag");f.type="text/javascript",f.async=!0,f.src=("https:"==t.location.protocol?"https://ssl.widgets.webengage.com":"http://cdn.widgets.webengage.com")+"/js/widget/webengage-min-v-5.0.js",d.parentNode.insertBefore(f,d)}}(window,document,"webengage");

		webengage.init(_YOUR_LICENSE_CODE_);
	</script>

This code loads and initializes WebEngage Widget asynchronously, so it does not block loading of your web page. This is cautiously done to not impact page load time on your website. The URLs in the above code are **protocol relative**. This lets the browser to load the widget over the same protocol (HTTP or HTTPS) as the containing page, which will prevent "Insecure Content" warnings.

### Initialization

**webengage.init** function initialises the widget by providing the `licenseCode` of the account. Once the **webengage-widget** initialises, it waits for `DOMContentLoaded` event until it modifies the dom for loading further resources, making sure it does not block the `DOMContentLoaded`.


### Configuration (webengage options)

All global and static configuration either at the widget level or individual product (Feedback, Survey or Notification) level can be set with **options** method.


## Widget Settings

### Widget Configuration Properties

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Property</th>
		<th>Type</th>
		<th>Description</th>
		<th>Default</th>
	</tr>
	<tr>
		<td valign="top">language* </td>
		<td valign="top">String</td>
		<td valign="top">Specify your widget language here. The static messages present in your feedback/survey form will appear in the language specified here.</td>
		<td valign="top">As per settings in the dashboard</td>
	</tr>
	<tr>
		<td valign="top">delay* </td>
		<td valign="top">number</td>
		<td valign="top">Delays API initialization for the time specified (in miliseconds).</td>
		<td valign="top">0</td>
	</tr>
	<tr>
		<td valign="top">defaultRender</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to false, then by default on widget initialization, none of the products (feedback, survey and notificaton) appears. To stop rendering selective products from rendering, set the **defaultRender** at the product level configuration properties. </td>
		<td valign="top">true</td>
	</tr>
	<tr>
		<td valign="top">customData</td>
		<td valign="top">object</td>
		<td valign="top">Specify your custom data here in proper JSON format to submit along with every survey / feedback submission and notification click</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">ruleData</td>
		<td valign="top">object</td>
		<td valign="top">Specify your rule data here in proper JSON format. The keys should match with the values used in Targeting rulese section for the corresponding Survey or Notification</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">tokens</td>
		<td valign="top">object</td>
		<td valign="top">Specify your tokens data here in proper JSON format.</td>
		<td valign="top">null</td>
	</tr>
</table>

** Property used only at the init time, changing this property later will have no effect*

#### Examples of using Widget Configuration Properties

##### Load WebEngage widget after 5 seconds

	webengage.options('delay', 5);

##### Pass your own custom/business data for tracking (e.g. userId, email, sessionId etc.)

	webengage.options('customData', { 'userId' : '2947APW09', 'age' : 21, 'isPaidCust' : false });

This data will be passed to all the activated products Survey,Feedback and Notification unless their own specific custom data (eg. `webengage.feedback.options('customData', { 'userId' : '2947APW09', 'age' : 21, 'isPaidCust' : false })` ) is provided.


##### Hide WebEngage by default on mobile and tablets

	var isMobile = {
		Android: function() {
				return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
				return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
				return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
				return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
				return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
				return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};

	if (isMobile.any()) {
		webengage.options('defaultRender', false);
	}

### Widget Callbacks

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Callback</th>
		<th>Type</th>
		<th>Description</th>
		<th>Default</th>
	</tr>
	<tr>
		<td valign="top">onReady*</td>
		<td valign="top">function</td>
		<td valign="top">Any code that you want to run after the API is loaded and initialized should be placed within the <tt class="docutils literal"><span class="pre">webengage.onReady</span></tt></td>
		<td valign="top">null</td>
	</tr>
</table>

#### Examples of using Widget Callback



##### Show a survey on click of a button instead of default rendering

	//default rendering is disabled
	webengage.survey.options('defaultRender', false);
	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function () {
			webengage.survey.render();
		};
	});


* [Other Platforms](other_platforms/readme.md)
