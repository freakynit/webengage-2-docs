#### Notification

## Notification Settings

### Notification Configuration Properties

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Property</th>
		<th>Type</th>
		<th>Description</th>
		<th>Default</th>
	</tr>
	<tr>
		<td valign="top">notificationId</td>
		<td valign="top">String</td>
		<td valign="top">To show a specific notification, specify the notificationId</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">skipRules</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to true, targeting rules specified in the dashboard for the active notifications are not executed. Thus, all the active notifications, unless notificationId is specified, become eligible to be shown.</td>
		<td valign="top">false</td>
	</tr>
	<tr>
		<td valign="top">forcedRender</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to true, the clicked or closed notifications also become elgible to be shown.</td>
		<td valign="top">false</td>
	</tr>
	<tr>
		<td valign="top">delay</td>
		<td valign="top">number</td>
		<td valign="top">Time delay, in miliseconds, to show a survey on a page. It overrides the time delay specified in the dashboard.</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">defaultRender</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to false, the eligible notification is not displayed by default.</td>
		<td valign="top">true</td>
	</tr>
	<tr>
		<td valign="top">customData</td>
		<td valign="top">object</td>
		<td valign="top">Specify your custom data for notification here in proper JSON format to submit along with every notification click. If not set then <tt class="docutils literal"><span class="pre">webengage.customData</span></tt> is referred</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">ruleData</td>
		<td valign="top">object</td>
		<td valign="top">Specify your rule data here in proper JSON format to filter you audience, with the keys you specified in the WebEngage Dashboard while editing rule for the corresponding Notification. If not specified <tt class="docutils literal"><span class="pre">webengage.ruleData</span></tt> is refered</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">tokens</td>
		<td valign="top">object</td>
		<td valign="top">Specify your tokens data here in proper JSON format. If not specified <tt class="docutils literal"><span class="pre">webengage.tokens</span></tt> is refered</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">enableCallbacks</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to true, will enable callbacks for all the notification events.</td>
		<td valign="top">false</td>
	</tr>
</table>

#### Examples of using Notification Properties

##### Delay a particular notification rendering

	webengage.notification.options({
		'delay' : 5,
		'notificationId' : '_NOTIFICATION_ID_'
	});

**Note** : If you have already set the time-delay in the notification rule while creating it, `delay` value pased in the **webengage.notification.options** will overwrite it.

##### Skip rules on a page for notifications

	webengage.notification.options('skipRules', true);

##### Custom rules - showing a "we are offline" notification every Sunday

	var daysInWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	webengage.notification.options('ruleData', {day : daysInWeek[new Date().getDay()]});

**Note**: In custom rules section, on the targeting page, you can use the "day" variable to create targeting rules. For example, if the day is "Sunday", display a notification on your contact page that your support team is currently offline.

### Notification Callbacks

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Callback</th>
		<th>Description</th>
		<th>Data</th>
	</tr>
	<tr>
		<td valign="top">onOpen</td>
		<td valign="top">invoked on notification open</td>
		<td valign="top">Data Object</td>
	</tr>
	<tr>
		<td valign="top">onClose</td>
		<td valign="top">invoked when a notification is closed</td>
		<td valign="top">Data Object</td>
	</tr>
	<tr>
		<td valign="top">onClick</td>
		<td valign="top">invoked when a notification is clicked</td>
		<td valign="top">Data Object</td>
	</tr>
</table>

####Notification onOpen callback data (JSON)

	{
		"notificationId": "173049892",
		"licenseCode": "311c48b3",
		"type": "notification",
		"title": "Checkout the new pricing",
		"event": "open",
		"activity": {
					"pageUrl": "http://webengage.net/",
					"pageTitle": "In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage",
					"referrer": "",
					"browser": "Chrome",
					"version": 27,
					"OS": "Windows",
					"country": "India",
					"region": "Maharashtra",
					"city": "Mumbai",
					"ip": "127.0.0.1"
			},
		"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			}
	}

####Notification onClick callback data (JSON)

	{
		"notificationId": "173049892",
		"licenseCode": "311c48b3",
		"type": "notification",
		"title": "Checkout the new pricing",
		"event": "click",
		"actionLink": "http://webengage.com/pricing",
		"actionText": "Check out",
		"activity": {
					"pageUrl": "http://webengage.net/",
					"pageTitle": "In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage",
					"referrer": "",
					"browser": "Chrome",
					"version": 27,
					"OS": "Windows",
					"country": "India",
					"region": "Maharashtra",
					"city": "Mumbai",
					"ip": "127.0.0.1"
			},
		"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			}
	}

####Notification onClose callback data (JSON)

On notification close the callback data for close event handlers will the latest event's (Open or Click) callback data that happened before close.
Lets say if you close a notification after it opens the callback data for close event would same as the onOpen callback data as mentioned above.

	{
		"notificationId": "173049892",
		"licenseCode": "311c48b3",
		"type": "notification",
		"title": "Checkout the new pricing",
		"event": "click",
		"actionLink": "http://webengage.com/pricing",
		"actionText": "Check out",
		"activity": {
					"pageUrl": "http://webengage.net/",
					"pageTitle": "In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage",
					"referrer": "",
					"browser": "Chrome",
					"version": 27,
					"OS": "Windows",
					"country": "India",
					"region": "Maharashtra",
					"city": "Mumbai",
					"ip": "127.0.0.1"
			},
		"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			}
	}

#### Examples of using Notification Callbacks

##### Callback handler when a notification is closed

	webengage.notification.onClose = function () {
		var r=confirm("Do you realy want to miss this offer");
		if (r==false){
		  return false;
		}
	};

### Notification Methods

**webengage.notification.render**

This method allows you to overwrite the dashboard settings. It also overwrites the settings provided in [_weq](#global-map-_weq) in the widget code. This method accepts all the [_weq notification configuration properties](#notification-configuration-properties) and [_weq callbacks](#notification-callbacks).


<table cellpading="1px" cellspacing="5px" width="50%" border="1px">
	<tr>
		<th>Name</th><th>Type</th><th>Description</th>
	</tr>
	<tr>
		<td valign="top">options</td>
		<td valign="top">Object</td>
		<td valign="top"><a href="#notification-configuration-properties">Notification configuration properties</a> &nbsp;<a href="#notification-callbacks">Callbacks</a>
		</td>
	</tr>
</table>

#### Examples of Render Method

##### Open a notification on click of a button

	webengage.notification.options('defaultRender', false);
	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function () {
			webengage.notification.render();
		};
	});

<table style="margin: 0; padding: 0; border: none;" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td style="margin: 0; padding: 0; border: none;">
			<button id="v4-btn-open-notification" class="cupid-green">Pop A Notification</button>
		</td>
	</tr>
</table>

##### Open a notification on page scroll (down)

	webengage.notification.options('defaultRender', false);
	var getScrollPercentage = function () {
		var documentHeight = $(document).height();
		var viewPortHeight = $(window).height();
		var verticlePageOffset = $(document).scrollTop();
		return Math.round(100* (viewPortHeight+verticlePageOffset)/documentHeight);
	}
	var notificationDisplayed = false;
	webengage.onReady(function(){
		window.onscroll = function (event) {
			if (!notificationDisplayed && getScrollPercentage() >= 70) {
				webengage.notification.render({ notificationId : '_NOTIFICATION_ID_' }); // invoking webengage.notification.render with specific notification id
				notificationDisplayed = true;
			}
		};
	});

Please scroll down a bit to see this in effect.
Also, notice that we are calling `webengage.notification.render` by passing it notificationId option.

##### Passing Custom Data / Rule Data

	webengage.notification.options('defaultRender', false);
	webengage.notification.options('ruleData', { 'categoryCode' '_CATEGORY_CODE_', 'paidCustomer' : true });
	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function () {
			webengage.notification.render({
				'customData' :  { 'userId' : '_USER_ID_', 'age' : _AGE_ }
			});
		};
	});


##### popping an alert onClose of a notification

	webengage.notification.options('defaultRender', false);
	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function(){
			webengage.notification.render().onClose( function(){
				alert("You have closed the notification");
			});
		};
	});

**webengage.notification.clear**

Clears rendered notification.

#### Examples of Clear Method

##### Clear a notification on click of a button

	webengage.notification.options('defaultRender', false);
	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function () {
			webengage.notification.clear();
		};
	});
