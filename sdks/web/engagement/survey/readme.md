#### Survey

## Survey Settings
### Survey Configuration Properties

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Property</th>
		<th>Type</th>
		<th>Description</th>
		<th>Default</th>
	</tr>
	<tr>
		<td valign="top">surveyId</td>
		<td valign="top">String</td>
		<td valign="top">To show a specific survey, specify the surveyId</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">skipRules</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to true, targeting rules specified in the dashboard for the active surveys are not executed. Thus, all the active surveys, unless surveyId is specified, become eligible to be shown.</td>
		<td valign="top">false</td>
	</tr>
	<tr>
		<td valign="top">forcedRender</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to true, the submitted or closed surveys also become elgible to be shown.</td>
		<td valign="top">false</td>
	</tr>
	<tr>
		<td valign="top">defaultRender</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to false, the eligible survey is not displayed by default.</td>
		<td valign="top">true</td>
	</tr>
	<tr>
		<td valign="top">delay</td>
		<td valign="top">number</td>
		<td valign="top">Time delay, in miliseconds, to show a survey on a page. It overrides the time delay specified in the dashboard.</td>
		<td valign="top">As set in the dashboard</td>
	</tr>
	<tr>
		<td valign="top">scope</td>
		<td valign="top">string/object</td>
		<td valign="top">A visitor life cycle depends on a long term cookie installed for your site in a browser. Lifecyle of a survey depends on the scope of a visitor. If a survey is closed, it doesn't appear for the visitor in the same browser session. If a survey is submitted, then it doesn't appear for the same visitor at all. If you want a survey to appear in every new browser session irrespective of the survey being taken in a past session, you can define your own scope. In this case, specify scope as SESSION_ID and the lifecycle of a survey remains within the scope of a session id. See examples below - </td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">scopeType</td>
		<td valign="top">string</td>
		<td valign="top">By defining custom scope, you can make a survey submitted once in that scope. By specifying the scopeType as 'session', you can narrow a scope within that session, making a possibility of a survey appearing multiple times to a visitor per each scope. By specifying the scopeType as 'global', you can make a survey submitted once per each scope value across different browser sessions/visitors.</td>
		<td valign="top">session</td>
	</tr>
	<tr>
		<td valign="top">customData</td>
		<td valign="top">object</td>
		<td valign="top">Specify your custom data for survey in proper JSON format to submit along with every survey submission. If not set then <tt class="docutils literal"><span class="pre">webengage.customData</span></tt> is referred</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">ruleData</td>
		<td valign="top">object</td>
		<td valign="top">Specify your rule data here in proper JSON format to filter you audience, with the keys you specified in the WebEngage Dashboard while editing rule for the corresponding Survey. If not specified <tt class="docutils literal"><span class="pre">webengage.ruleData</span></tt> is refered</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">enableCallbacks</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to true, will enable callbacks for all the survey events.</td>
		<td valign="top">false</td>
	</tr>
</table>

#### Examples of using Survey Properties

##### Pass business data for custom rules

	webengage.survey.options('ruleData', {
		'itemsInCart' : 3,
		'cartAmount' : 1288,
		'customerType' : 'gold'
	});

##### Show surveys to a visitor everytime a new browser session starts
By specifying a session_id (some unique identifier for a browser session) as the survey scope with scopeType as 'session', one can make a survey re-appear to a visitor, even if (s)he has closed or submitted it in a previous browser session.

	webengage.survey.options({
		'scope' : '_USER_SESSION_ID_',
		'scopeType' : 'session'
	});

**Note** : Notice that **options** method either takes key value as input or javascript map.


##### Show a survey to a visitor every day irrespective (s)he has closed/submitted the same survey.
By specifying a today's date as the survey scope, one can make a survey re-appear to a visitor each day even if he has closed or submitted it.

	var today = new Date();
	webengage.survey.options('scope', {
		'scope' : (today.getDate()+"-"+today.getMonth()+"-"+today.getYear()),
		'scopeType' : 'session',
		'surveyIds' : ["~29aj48l"]
	});

##### Show a survey once to a logged in user from differernt browsers
If one wants a survey to be submitted once per logged in user irrespective of different browser sessions, then specify logged in user's email or userId as the scope with scopeType as 'global'.

	webengage.survey.options({
		'scope' : '_USER_EMAIL_',
		'scopeType' : 'global'
	});


### Survey Callbacks

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Callback</th>
		<th>Description</th>
		<th>Callback Data</th>
	</tr>
	<tr>
		<td valign="top">onOpen</td>
		<td valign="top">invoked on survey open</td>
		<td valign="top">Object</td>
	</tr>
	<tr>
		<td valign="top">onClose</td>
		<td valign="top">invoked when a survey is closed</td>
		<td valign="top">Object</td>
	</tr>
	<tr>
		<td valign="top">onSubmit</td>
		<td valign="top">invoked when a question is answered</td>
		<td valign="top">Object</td>
	</tr>
	<tr>
		<td valign="top">onComplete</td>
		<td valign="top">invoked when the thank you message is displayed in the end</td>
		<td valign="top">Object</td>
	</tr>
</table>

#### Survey onOpen callback data (JSON)

	{
			"surveyId": "~5g1c4fd",
			"licenseCode": "311c48b3",
			"activity": {
					"pageUrl": "http://webengage.net/",
					"pageTitle": "In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage",
					"referrer": "",
					"browser": "Chrome",
					"version": 27,
					"OS": "Windows",
					"country": "India",
					"region": "Maharashtra",
					"city": "Mumbai",
					"ip": "127.0.0.1"
			},
			"type": "survey",
			"title": "Survey #1",
			"totalQuestions": 6,
			"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			}
	}

#### Survey onSubmit callback data (JSON)

	{
			"surveyId": "~5g1c4fd",
			"licenseCode": "311c48b3",
			"activity": {
					"pageUrl": "http://webengage.net/",
					"pageTitle": "In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage",
					"referrer": "",
					"browser": "Chrome",
					"version": 27,
					"OS": "Windows",
					"country": "India",
					"region": "Maharashtra",
					"city": "Mumbai",
					"ip": "127.0.0.1"
			},
			"type": "survey",
			"title": "Survey #1",
			"totalQuestions": 6,
			"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			},
			"questionResponses": [{
					"questionId": "~1fstl7a",
					"questionText": "What is your favourite color?",
					"value": {
							"@class": "list",
							"values": ["Red"]
					}
			}]
	}

#### Survey onComplete callback data (JSON)

	{
			"surveyId": "~5g1c4fd",
			"licenseCode": "311c48b3",
			"activity": {
					"pageUrl": "http://webengage.net/",
					"pageTitle": "In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage",
					"referrer": "",
					"browser": "Chrome",
					"version": 27,
					"OS": "Windows",
					"country": "India",
					"region": "Maharashtra",
					"city": "Mumbai",
					"ip": "127.0.0.1"
			},
			"type": "survey",
			"title": "Survey #1",
			"totalQuestions": 6,
			"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			},
			"questionResponses": [
			{
					"questionId": "~1fstl7a",
					"questionText": "Your name?",
					"value": {
							"@class": "map",
							"values": {
									"First name": "ABC",
									"Last name": "EFG"
							}
					}
			},
			{
					"questionId": "~1asf233",
					"questionText": "Enter your mobile number?",
					"value": {
							"@class": "text",
							"text": "9988776655"
					}
			},
			{
					"questionId": "we2e4545",
					"questionText": "Your Bio?",
					"value": {
							"@class": "text",
							"text" : "I am an early beta user of this product."
					}
			},
			{
					"questionId": "324saf3w",
					"questionText": "What is your favourite color?",
					"value": {
							"@class": "list",
							"values": ["Red"]
					}
			},
			{
					"questionId": "~213wrw43s",
					"questionText": "Which countries you have been to?",
					"value": {
							"@class": "list",
							"values": [ "US", "Mexico" ]
					}
			},
			{
					"questionId": "~ew347fev"
					"questionText": "Rate out website?",
					"value": {
							"@class" : "matrix",
							"values" : {
									"Experience" : [ "Good" ],
									"Content" : [ "Good" ],
									"Design" : [ "Poor" ]
							}
					}
			},
			{
					"questionId": "sht4k8",
					"questionText": "Upload your resume?",
					"value": {
							"@class" : "file",
							"fileName" : "android_sdk_developing.pdf",
							"fileSize" : 1919972,
							"fileAccessUrl" : "https://api.webengage.com/v1/download/feedback/response/ofq4jy",
							"fileType" : "pdf"
					}
			}]
	}

#### Survey onClose callback data (JSON)
On survey close the callback data for close event handlers will the latest event's (Open, Submit or Complete) callback data that happened before close.
Lets say if you close a survey after it opens the callback data for close event would same as the onOpen callback data as mentioned above.

	{
			"surveyId": "~5g1c4fd",
			"licenseCode": "311c48b3",
			"activity": {
					"pageUrl": "http://webengage.net/",
					"pageTitle": "In-site customer Feedback, targeted Surveys & push Notifications for Websites - WebEngage",
					"referrer": "",
					"browser": "Chrome",
					"version": 27,
					"OS": "Windows",
					"country": "India",
					"region": "Maharashtra",
					"city": "Mumbai",
					"ip": "127.0.0.1"
			},
			"type": "survey",
			"title": "Survey #1",
			"totalQuestions": 6,
			"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			}
	}

#### Examples of using Survey Callbacks

##### Close survey after certain response of a particular question

	var _weq = _weq || {};
	_weq['webengage.licenseCode'] = '_LICENSE_CODE_';
	_weq['webengage.enableCallbacks'] = true;
	_weq['webengage.survey.onSubmit'] = function (data) {
		var surveyInstance = this;
		if(data !== undefined && data.questionResponses !== undefined && data.questionResponses.length > 0) {
			for(var i=0; i < data.questionResponses.length; i++) {
				var question = data.questionResponses[i];
				if(question["questionId"] === "324saf3w") {
					var questionResponse = question["value"];
					var values = questionResponse["values"];
					if(values !== 'undefined' &&  values.length > 0){
						for(var k=0; k < values.length; k++) {
							if(values[k] === 'RED') {
								surveyInstance.close();
								break;
							}
						}
					}
					break;
				}
			}
		}
	};

##### Redirect a user on survey completion

	webengage.survey.onComplete = function () {
		window.location.href='/newletter/subscribe.html';
	};

### Survey Methods

**webengage.survey.render**

This method allows you to overwrite the dashboard settings. It also overwrites the settings provided in [_weq](#global-map-_weq) in the widget code. This method accepts all the [_weq survey configuration properties](#survey-configuration-properties) and [_weq callbacks](#survey-callbacks).

<table cellpading="1px" cellspacing="5px" width="50%" border="1px">
	<tr>
		<th>Name</th><th>Type</th><th>Description</th>
	</tr>
	<tr>
		<td valign="top">options</td>
		<td valign="top">Object</td>
		<td valign="top"><a href="#survey-configuration-properties">Survey configuration properties</a> &nbsp; <a href="#survey-callbacks">callbacks</a>
</td>
	</tr>
</table>

#### Examples of Render Method

##### Open a survey on click of a button

	webengage.survey.options({
		'surveyId' : '_SURVEY_ID_', // survey id to be rendered ... note : it is optional
		'defaultRender' : false
	});
	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function () {
			webengage.survey.render();
		};
	});

<table style="margin: 0; padding: 0; border: none;" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td style="margin: 0; padding: 0; border: none;">
			<button id="v4-btn-open-survey" class="cupid-green">Pop A Survey</button>
		</td>
	</tr>
</table>

##### Open a survey on page scroll (down)

	webengage.survey.options({
		'defaultRender' : false
	});
	(function () {
		var getScrollPercentage = function () {
			var documentHeight = $(document).height();
			var viewPortHeight = $(window).height();
			var verticlePageOffset = $(document).scrollTop();
			return Math.round(100* (viewPortHeight+verticlePageOffset)/documentHeight);
		}
		var surveyDisplayed = false;
		webengage.onReady(function(){
			window.onscroll = function (event) {
				if (!surveyDisplayed && getScrollPercentage() >= 70) {
					webengage.survey.render({ surveyId : '_SURVEY_ID_' }); // invoking webengage.survey.render with specific survey id
					surveyDisplayed = true;
				}
		}
		});
	})();

Please scroll down, till the end to see this in effect.
Also, notice that we are calling `webengage.survey.render` by passing it surveyId option.

##### Passing Custom Data / Rule Data

	webengage.survey.options({
		'defaultRender' : false
	});
	webengage.survey.options('ruleData', { 'categoryCode' '_CATEGORY_CODE_', 'paidCustomer' : true });
	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function () {
			webengage.survey.render({
				'customData' :  { 'userId' : '_USER_ID_', 'age' : _AGE_ }
			});
		};
	});



##### Track survey view in console

	webengage.survey.options({
		'defaultRender' : false
	});
	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function(){
			webengage.survey.render().onOpen( function(){
				console.log("survey open");
			});
		};
	});

**webengage.survey.clear**

Clears rendered survey.

#### Examples of Clear Method

##### Clear a survey on click of a button

	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function () {
			webengage.survey.clear();
		};
	});
