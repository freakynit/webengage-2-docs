#### Feedback

## Feedback Settings

### Feedback Configuration Properties

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Property</th>
		<th>Type</th>
		<th>Description</th>
		<th>Default</th>
	</tr>
	<tr>
		<td valign="top">delay</td>
		<td valign="top">number</td>
		<td valign="top">Time delay, in miliseconds, to show the feedback tab on a page.</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">formData</td>
		<td valign="top">Array of <a href="#formdata-object">Object</a></td>
		<td valign="top">Specify values in the feedback form for the pre-population of the fields. <a href="#prepopulate-email-hide-message-select-category-and-keep-it-readonly">example</a></td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">alignment</td>
		<td valign="top">String</td>
		<td valign="top">Shows the feedback tab on the left/right side of the webpage. The possible value that you can specify here is <strong>left</strong> or <strong>right</strong>.</td>
		<td valign="top">As specified in feedback tab configuration</td>
	</tr>
	<tr>
		<td valign="top">borderColor</td>
		<td valign="top">String</td>
		<td valign="top">Renders feedback tab with the specified border color</td>
		<td valign="top">As specified in feedback tab configuration</td>
	</tr>
	<tr>
		<td valign="top">backgroundColor</td>
		<td valign="top">String</td>
		<td valign="top">Renders feedback tab with the specified background color</td>
		<td valign="top">As specified in feedback tab configuration</td>
	</tr>
	<tr>
		<td valign="top">defaultCategory</td>
		<td valign="top">String</td>
		<td valign="top">Shows the feedback form based on the feedback category specified here.</td>
		<td valign="top">As specified in feedback category configuration</td>
	</tr>
	<tr>
		<td valign="top">showAllCategories</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to <strong>true</strong>, the feedback form appears with feedback category dropdown menu to let the end users to submit contextual feedbacks. If set to <strong>false</strong>, the feedback form appears based on the default feedback category specified without any feedback category dropdown menu.</td>
		<td valign="top">true</td>
	</tr>
	<tr>
		<td valign="top">showForm</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to <strong>true</strong>, feedback form slides out, on page load, by default.</td>
		<td valign="top">false</td>
	</tr>
	<tr>
		<td valign="top">defaultRender</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to false, the feedback tab is not displayed by default.</td>
		<td valign="top">true</td>
	</tr>
	<tr>
		<td valign="top">customData</td>
		<td valign="top">Object</td>
		<td valign="top">Specify your custom data here in proper JSON format to submit along with every feedback got submitted, if not specified <tt class="docutils literal"><span class="pre">webengage.data</span></tt> is referred.</td>
		<td valign="top">null</td>
	</tr>
	<tr>
		<td valign="top">enableCallbacks</td>
		<td valign="top">boolean</td>
		<td valign="top">If set to true, will enable callbacks for all the feedback events.</td>
		<td valign="top">false</td>
	</tr>
</table>

#### formData Object

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Property</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
	<tr>
		<td valign="top">name</td>
		<td valign="top">String</td>
		<td valign="top">Label of the field in the feedback form which you want to prepopulate data or want to hide or make it mandatory</td>
	</tr>
	<tr>
		<td valign="top">value</td>
		<td valign="top">String</td>
		<td valign="top">Spefy the value of the field</td>
	</tr>
	<tr>
		<td valign="top">mode</td>
		<td valign="top">String</td>
		<td valign="top">Specify if the field should be hidden, read only or default (no-change).  So values only supported are <tt class="docutils literal"><span class="pre">hidden</span></tt> and <tt class="docutils literal"><span class="pre">readOnly</span></tt> and <tt class="docutils literal"><span class="pre">default</span></tt></td>
	</tr>
	<tr>
		<td valign="top">isMandatory</td>
		<td valign="top">Boolean</td>
		<td valign="top">Specify if the field should be mandatory or not.</td>
	</tr>
	<tr>
		<td valign="top">options</td>
		<td valign="top">String Array</td>
		<td valign="top">Applicable only in case of 'Category' field. Specify in case you want to show specific values in the category dropdown.</td>
	</tr>
</table>

**NOTE** : To see the example code click [here](#prepopulate-email-hide-message-select-category-and-keep-it-readonly)

#### Examples of using Feedback Properties

##### Set feedbackTab alignment to right

	webengage.feedback.options('alignment', 'right');

##### Set feedbackTab background colours and border colours

	webengage.feedback.options({
		'backgroundColor' : '#ff9',
		'borderColor' : '#f99'
	});


##### Prepopulate email and keep it readonly, hide message, specific options for category drop-down
	// Values int the category drop-down 'Like' & 'Question'

	webengage.feedback.options('formData', [
		{
			'name' : 'email',
			'value' : '_EMAIL_VALUE_',
			'mode' : 'readOnly'
		}, {
			'name' : 'message',
			'mode' : 'hidden'
		}, {
			'name' : 'category',
			'isMandatory' : false,
			'value' : 'like',
			'options' : ['Like', 'Question'] // make sure you send right category lables
		}
	]);

<table style="margin: 0; padding: 0; border: none;" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td style="margin: 0; padding: 0; border: none;">
			<button id="v4-btn-feedback-pre-populated" class="cupid-green">Prepopulate Feedback Fields</button>
		</td>
	</tr>
</table>


### Feedback Callbacks

<table cellpading="1px" cellspacing="5px" border="1px">
	<tr>
		<th>Callback</th>
		<th>Description</th>
		<th>Data</th>
	</tr>
	<tr>
		<td valign="top">onOpen</td>
		<td valign="top">invoked on survey open</td>
		<td valign="top">Data Object</td>
	</tr>
	<tr>
		<td valign="top">onClose</td>
		<td valign="top">invoked when a survey is closed</td>
		<td valign="top">Data Object</td>
	</tr>
	<tr>
		<td valign="top">onSubmit</td>
		<td valign="top">invoked when a question is answered</td>
		<td valign="top">Data Object</td>
	</tr>
</table>

####Feedback onOpen callback data (JSON)

	{
			"licenseCode": "311c48b3",
			"type": "feedback",
			"activity": {
			"pageUrl": "http://webengage.net/publisher/feedback-configuration/fields/311c48b3",
			"pageTitle": "Feedback Configuration - WebEngage",
			"ip": "127.0.0.1",
			"city": "Mumbai",
			"country": "India",
			"browser": "Firefox",
			"browserVersion": "18",
			"platform": "Linux",
			"activityOn": "2013-02-11T08:09+0000"
		},
			"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			}
	}

####Feedback onSubmit callback data (JSON)

	{
		"id": "has7e2",
		"licenseCode": "311c48b3",
		"type": "feedback",
		"fields": [{
			"id": "1fcdjgf",
			"label": "Name",
			"type": "default",
			"value": {
				"@class": "name",
				"text": "John"
			}
		}, {
			"id": "ah1ihjd",
			"label": "Email",
			"type": "default",
			"value": {
				"@class": "email",
				"text": "john@doe.com"
			}
		}, {
			"id": "cci1868",
			"label": "Category",
			"type": "default",
			"value": {
				"@class": "category",
				"text": "Suggestion"
			}
		}, {
			"id": "~78me196",
			"label": "Message",
			"type": "default",
			"value": {
				"@class": "message",
				"text": "Thank you very much for this awesome service!"
			}
		}, {
			"id": "~5d68amb",
			"label": "Attach a screenshot of this page",
			"type": "default",
			"value": {
				"@class": "snapshot",
				"thumbnailImageUrl": ""
			}
		}, {
			"id": "n283q0",
			"label": "Enter your mobile number",
			"type": "custom",
			"value": {
				"@class": "text",
				"text": "9988776655"
			}
		}, {
			"id": "pp3j84",
			"label": "Your Bio",
			"type": "custom",
			"value": {
				"@class": "text",
				"text": "I am an early beta user of this product."
			}
		}, {
			"id": "19jb68o",
			"label": "Which countries you have been to?",
			"type": "custom",
			"value": {
				"@class": "list",
				"values": ["US", "Mexico"]
			}
		}, {
			"id": "1cc6lks",
			"label": "Rate our website",
			"type": "custom",
			"value": {
				"@class": "matrix",
				"values": {
					"Experience": ["Good"],
					"Content": ["Good"],
					"Design": ["Poor"]
				}
			}
		}, {
			"id": "sht4k8",
			"label": "Upload your resume",
			"type": "custom",
			"value": {
				"@class": "file",
				"fileName": "android_sdk_developing.pdf",
				"fileSize": 1919972,
				"fileAccessUrl": "https://api.webengage.com/v1/download/feedback/response/ofq4jy",
				"fileType": "pdf"
			}
		}, {
			"id": "16qfkqk",
			"label": "What is your favourite color?",
			"type": "custom",
			"value": {
				"@class": "list",
				"values": ["Red"]
			}
		}],
		"activity": {
			"pageUrl": "http://webengage.net/publisher/feedback-configuration/fields/311c48b3",
			"pageTitle": "Feedback Configuration - WebEngage",
			"ip": "127.0.0.1",
			"city": "Mumbai",
			"country": "India",
			"browser": "Firefox",
			"browserVersion": "18",
			"platform": "Linux",
			"activityOn": "2013-02-11T08:09+0000"
		},
		"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			}
	}
####Feedback onClose callback data (JSON)

On feedback close the callback data for close event handlers will the latest event's (Open or Submit) callback data that happened before close.
Lets say if you close feedback window after it opens the callback data for close event would same as the onOpen callback data as mentioned above.

	{
			"licenseCode": "311c48b3",
			"type": "feedback",
			"activity": {
			"pageUrl": "http://webengage.net/publisher/feedback-configuration/fields/311c48b3",
			"pageTitle": "Feedback Configuration - WebEngage",
			"ip": "127.0.0.1",
			"city": "Mumbai",
			"country": "India",
			"browser": "Firefox",
			"browserVersion": "18",
			"platform": "Linux",
			"activityOn": "2013-02-11T08:09+0000"
		},
			"customData": {
					"userName" : [ "john" ],
					"gender" : [ "male" ],
					"customerType" : [ "Gold" ],
					"jsessionid" : [ "CB300FEC898236FF9E5A181E468CA6BC" ]
			}
	}


#### Examples of using Feedback Callbacks

##### Close the feedback window in 5 second just after someone submits feedback.

	webengage.feedback.onSubmit = function () {
			var feedbackInstance = this;
			setTimeout(function(){
				feedbackInstance.close();
			}, 5000);
	};

### Feedback Methods

**webengage.feedback.render**

This method allows you to overwrite the dashboard settings. It also overwrites the settings provided in [_weq](#global-map-_weq) in the widget code. This method accepts all the [_weq feedback configuration properties](#feedback-configuration-properties) and [_weq callbacks](#feedback-callbacks).


<table cellpading="1px" cellspacing="5px" width="50%" border="1px">
	<tr>
		<th>Name</th><th>Type</th><th>Description</th>
	</tr>
	<tr>
		<td valign="top">options</td>
		<td valign="top">Object</td>
		<td valign="top"><a href="#feedback-configuration-properties">Feedback Configuration Properties</a> &amp;<a href="#feedback-callbacks">Callbacks</a>
		</td>
	</tr>
</table>

#### Examples for Render Method

##### Show a feedback on click of a button

	webengage.onReady(function(){
		document.getElementById("_BUTTON_ID_").onclick = function () {
			webengage.feedback.render({ 'showForm' : true });
		};
	});

<table style="margin: 0; padding: 0; border: none;" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td style="margin: 0; padding: 0; border: none;">
			<button id="v4-btn-open-feedback" class="cupid-green">Open Feedback Form</button>
		</td>
	</tr>
</table>

#### Example of clear Method

##### Clear a feedback on click of a button

	document.getElementById("_BUTTON_ID_").onclick = function () {
		webengage.feedback.clear();
	};
