#### Events

## Event Tracking

Event tracking enables you to track data and trigger event on the basis of which one can using the following method.

### Event Method

**webengage.track**

This method allows you to pass business events to WebEngage for analytics or to trigger surveys/notifications.

All passed events are captured in the event analytics screen (coming soon)

#### Parameters
<table cellpading="1px" cellspacing="5px" width="50%" border="1px">
	<tr>
		<th>Name</th><th>Type</th><th>Description</th>
	</tr>
	<tr>
		<td valign="top">eventName</td>
		<td valign="top">String</td>
		<td valign="top">Name of the event that occurred</td>
	</tr>
	<tr>
		<td valign="top">eventData</td>
		<td valign="top">Object</td>
		<td valign="top">Data to associate with the event. Pass a simple object with only strings, numbers, booleans and dates as property values</td>
	</tr>
</table>

#### Example of a webengage.track call

##### Let WebEngage know when a visitor leaves a cart before checkout
```javascript
	webengage.track('Added To Cart', {
        productId: 1337,
        product: 'Givenchy Pour Homme Cologne',
        category: 'Fragrance',
        price: 39.80,
        currency: 'USD',
        quantity: 1
    });
```
