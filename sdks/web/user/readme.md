#### User

## User Tracking

User tracking API's lets you to attach attributes to logged in as well as anonymous users. Attributes are set of properties that generates user profiles. This profile information can be used to segment users, which can be then used for better targeting and for getting user analytics in the WebEngage Dashboard.

### User tracking Method


```javascript
webengage.user.identify
```

This method lets you inform WebEngage about identified user and accepts user-idetification id as the input.

#### Parameters
<table cellpading="1px" cellspacing="5px" width="50%" border="1px">
	<tr>
		<th>Name</th><th>Type</th><th>Description</th>
	</tr>
	<tr>
		<td valign="top">userId</td>
		<td valign="top">String</td>
		<td valign="top">User identifier you want to track with WebEngage</td>
	</tr>
</table>


```javascript
webengage.user.login
```

This method like **identify** method, lets you inform WebEngage about logged in user and accepts user-idetification id as the input.

#### Parameters
<table cellpading="1px" cellspacing="5px" width="50%" border="1px">
	<tr>
		<th>Name</th><th>Type</th><th>Description</th>
	</tr>
	<tr>
		<td valign="top">userId</td>
		<td valign="top">String</td>
		<td valign="top">User identifier you want to track with WebEngage</td>
	</tr>
</table>


```javascript
webengage.user.logout
```

This method lets you inform WebEngage that current user has logged out and treat the use as anonymous user from this point onwards. This method does not expect any parameters.


```javascript
webengage.user.setAttribute
```

This method lets you set user attributes for a logged-in user. This is useful when setting targeting rules for Surveys/Notification in the dasbhoard and also useful for user/segment analytics. This method accepts **key** `attribute-name` and **value** `attribute-value` as parameter and it also accetps an **object** of `attribute-name` and `attribute-value` as input.

#### Parameters when setting one attribute
<table cellpading="1px" cellspacing="5px" width="50%" border="1px">
	<tr>
		<th>Name</th><th>Type</th><th>Description</th>
	</tr>
	<tr>
		<td valign="top">attribute-name</td>
		<td valign="top">String</td>
		<td valign="top">Name of the attribute</td>
	</tr>
	<tr>
		<td valign="top">attribute-value</td>
		<td valign="top">String</td>
		<td valign="top">Value of the attribute</td>
	</tr>
</table>

#### Parameters when setting multiple attributes
<table cellpading="1px" cellspacing="5px" width="50%" border="1px">
	<tr>
		<th>Name</th><th>Type</th><th>Description</th>
	</tr>
	<tr>
		<td valign="top">attribute-key-value-object</td>
		<td valign="top">Object</td>
		<td valign="top">Attribute key value object</td>
	</tr>
</table>


##### Predefined attribute keys

<table cellpading="1px" cellspacing="5px" width="50%" border="1px">
	<tr>
		<th>Name</th><th>Type</th><th>Description</th>
	</tr>
	<tr>
		<td valign="top">email</td>
		<td valign="top">String</td>
		<td valign="top">User Email address</td>
	</tr>
	<tr>
		<td valign="top">birth_date</td>
		<td valign="top">String</td>
		<td valign="top">User's birth-date as `yyyy-mm-dd` format</td>
	</tr>
	<tr>
		<td valign="top">gender</td>
		<td valign="top">String</td>
		<td valign="top">User's gender `male`, `female` or `other`</td>
	</tr>
	<tr>
		<td valign="top">first_name</td>
		<td valign="top">String</td>
		<td valign="top">User's first name</td>
	</tr>
	<tr>
		<td valign="top">last_name</td>
		<td valign="top">String</td>
		<td valign="top">User's last name</td>
	</tr>
	<tr>
		<td valign="top">company</td>
		<td valign="top">String</td>
		<td valign="top">User's company</td>
	</tr>
</table>
