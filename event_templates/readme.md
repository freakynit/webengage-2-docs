#### Event Templates

| Event Name         | Description                                              | Attributes- Sample values                                                                                       |
|--------------------|----------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
| App Start          | Track how users launch your app.                         | Source    - Push, PPC, Organic, Direct, Email                                                                   |
| Registered orLogin | Track which medium the userused to login or register.    | Channel     - Facebook, Email, Twitter                                                                          |
| Social Action      | Track social media eventsperformed by users in your app. | Platform <br /> - Twitter, Facebook  <br /><br />Action <br /> - Share, Like  <br /><br />Content <br /> - Article or content or user identifier |
| App Exit          | Track users exiting from the app.                         | Source    - Push, PPC, Organic, Direct, Email                                                                   |
| App Error          | Track known application errors.                         | Type <br /> - Network error, No privilege, Bad HTTP response                                                                  |
| App Crash          | Track app crashes.                                       | Type                                                                   |
