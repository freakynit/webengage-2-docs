#### WebHooks

Welcome to WebEngage WebHooks. WebHook lets you add a "HTTP callback" to the events happening in WebEngage, related to your **Feedback**, **Surveys** and **Notifications**. By configuring a WebHook, WebEngage is triggered to call a script on your web-servers whenever a particular event, for which you want to get a real-time data, occurs and use it for internal purpose. For example, real-time pushing feedback and replies from WebEngage to internal CRM system or triggering your own business logic. Currently, WebEngage Feedback related WebHooks can be configured for these 2 events - ``FEEDBACK_SUBMIT``, ``FEEDBACK_REPLY_SUBMIT`` and ``SURVEY_RESPONSE``.

(Coming soon) We will soon be adding other events for which you can get WebHooks, such as ``NOTIFICATION_CLICK`` etc.

## Overview

* [How to configure a WebHook ?](#configuration)
* [What data does a WebHook HTTP trigger contain ?](#webhook-requests)
* [How to authenticate a WebEngage WebHook request ?](#verifying-request)
* [What happens if the request fails ?](#error-handling-and-retries)
<!-- * [What are the best practices](#best-practices) -->

## Configuration

In your WebEngage Dashboard, you will be able to configure a URLs for different events. Following are the WebHooks we currently allow you to configure.

**Global WebHook**

You can choose to have a global URL for all the events, by specifying the postURL in the the Global WebHook and along with the response format, from ``xml`` & ``json``. **Global WebHook** also acts as fallback WebHook when an event specific WebHook is not specified.

**Feedback WebHook**

Triggered on feedback submission. To add a WebHook specific to ``FEEDBACK_SUBMIT`` event, you need to configure this WebHook by specifying the postURL and the response format again from ``xml`` & ``json``. If specified, it gets preference over **Global WebHook**.

**Feedback Reply WebHook**

Triggered on posting a reply to an existing feedback thread. To add a WebHook specific to ``FEEDBACK_REPLY_SUBMIT`` event, you need to configure this WebHook by specifying the postURL and the response format again from ``xml`` & ``json``. If specified, it gets preference over **Global WebHook**.

**Survey Response WebHook**

Triggered on survey response submission. To add a WebHook specific to ``SURVEY_RESPONSE_SUBMIT`` event, you need to configure this WebHook by specifying the postURL and the response format again from ``xml`` & ``json``. If specified, it gets preference over **Global WebHook**.

## WebHook Requests

WebHook requests will be posted to a configured URL and format for a specific [WebHook](#configuration). WebHook request method will always be ``POST``. There will be additional parameters that will be appended to the postURL.

**Parameters**

<table cellpadding="5px" cellspacing="5px" width="80%">
	<tr>
		<th>Name</th>
		<th>Description</th>
	</tr>
	<tr>
		<td valign="top">eventType</td>
		<td valign="top">Event for which data is being posted</td>
	</tr>
	<tr>
		<td valign="top">licenseCode</td>
		<td valign="top">Your WebEngage Publisher LicenseCode</td>
	</tr>
	<tr>
		<td valign="top">secret</td>
		<td valign="top">This secret key is to be used to identify the webHook post requests from WebEngage. More about this <a href="#verifying-request" class="reference external">here</a></td>
	</tr>
</table>

**Request Body**

Request body will contain the event specific data. Currently supported WebHooks

* ``FEEDBACK_SUBMIT`` event - Example of how the feedback data looks like. [XML](http://docs.webengage.com/api/rest-api.html#xml-response) [JSON](http://docs.webengage.com/api/rest-api.html#json-response)

* ``FEEDBACK_REPLY_SUBMIT`` event - Example of how the feedback reply data looks like. [JSON](http://docs.webengage.com/api/rest-api.html#get-a-feedback-reply)

* ``SURVEY_RESPONSE_SUBMIT`` event - Example of how the survey response data looks like. [XML](http://docs.webengage.com/api/rest-api.html#xml-response) [JSON](http://docs.webengage.com/api/rest-api.html#json-response)

**Expected Response**

WebEngage will understand ``HTTP Status``, of the response. For ``HTTP Status`` 200 will be treated as Successful posts, everything else will be logged as failed WebHook posts.
See [this](#error-handling-and-retries) to understand, what happens in case of failures.

## Verifying Request

You will get a **WebHook Secret Key** for your account on the **WebHook Configuration** page. This secret key is to be used to identify the WebHook post requests from WebEngage to your servers. WebEngage appends ``secret`` parameter in the postURL [see here](#webhook-requests). Value of the ``secret`` parameter is the **MD5 Hex** of the combination of the ``licenseCode`` and the **WebHook Secret Key** key separated by a ``:``.

## Error Handling and Retries

We keep a log of recent **WebHook Requests** under **WebHook History** tab in the dashboard. We currently do not support automatic retries. To see the logs and manage failed requests, you will need to go to your **WebHook History** from there you will be able to resend a particular failed **WebHook Requests** by clicking the ``resend`` link against that history item.


<!-- ## Best Practices -->
